<?php
/*
 this class is the class that makes up our data access layer.  this class is responsible for inserting, updating,
and deleting data from the database
 */

class ThrowdownDAO
{
    private $mysqli;
    //this constructor class is resonsible for setting our data access layer with the database credentials
    function __construct()
    {
        $databaseInformation = DatabaseUtil::getDatabaseConfigurationDetails();
        if ($databaseInformation == null)
            return;
        $this->site_dbServer = $databaseInformation->server;
        $this->site_dbName = $databaseInformation->database;
        $this->site_dbUser = $databaseInformation->username;
        $this->site_dbPassword = $databaseInformation->password;
        $mysqli = new mysqli($this->site_dbServer, $this->site_dbUser, $this->site_dbPassword, $this->site_dbName);
        $this->mysqli = $mysqli;
    }

    //this method is responsible for adding new user data to the user table
    public function addUserAccount($username, $country){

        $added = new Added();

        $query="INSERT INTO user(username, country) VALUES (?,?)";
        $stmt = $this->mysqli->prepare($query);
        if($stmt){
            $stmt->bind_param('ss', $username, $country);
            $stmt->execute();
            $insertId = $stmt->insert_id;
            $stmt->free_result();
            $stmt->close();

            $added->id = $insertId;
        }

        return $added;
    }
    //this method is responsible for getting a user by user id
    public function getUserByUserId($userId){
        $user = null;
        $query = "SELECT * FROM user WHERE id = ?";
        $stmt = $this->mysqli->prepare($query);
        if($stmt){
            $stmt->bind_param('i', $userId);
            $stmt->execute();
            $stmt->bind_result($id,$username, $joinDate, $country);
            $stmt->fetch();

            $user = new User();
            $user->id = $id;
            $user->userName = $username;
            $user->joinDate = $joinDate;
            $user->country = $country;

            $stmt->free_result();
            $stmt->close();
        }

        return $user;
    }
    //this method is responsible for returning a count of users in the user table that have a certain username
    public function countUserByUsername($username){

        $itemCount = 0;
        $query = "SELECT COUNT(*) AS Item_Count FROM user WHERE username = ?";
        $stmt = $this->mysqli->prepare($query);
        if($stmt)
        {
            $stmt->bind_param('s', $username);
            $stmt->execute();
            $stmt->bind_result($itemCount);
            $stmt->fetch();
            $stmt->store_result();
            $stmt->close();
            $stmt = null;
        }
        return $itemCount;
    }
    //this method is responsible for updating the user table by user id
    public function updateUser($id, $country, $userName){

        $updated = new Updated();
        $query = "UPDATE user SET country=(?), username=(?) WHERE id = (?)";
        $stmt = $this->mysqli->prepare($query);
        if($stmt){
            $stmt->bind_param('ssi', $country, $userName, $id);
            $stmt->execute();
            $number_affected = $stmt->affected_rows;
            $stmt->free_result();
            $stmt->close();
            $updated->id = $id;
            $updated->numberUpdated = $number_affected;
        }

        return $updated;
    }
    //this method is responsible for adding user throw data to the leaderboard table
    public function addUserThrowDataToLeaderBoard($userId, $score, $throwDuration){

        $added = new Added();

        $query="INSERT INTO leaderboard(user_id, score, throwduration) VALUES (?,?,?)";
        $stmt = $this->mysqli->prepare($query);
        if($stmt){
            $stmt->bind_param('iii', $userId, $score, $throwDuration);
            $stmt->execute();
            $insertId = $stmt->insert_id;
            $stmt->free_result();
            $stmt->close();

            $added->id = $insertId;
        }

        return $added;
    }
    //this method is responsible for adding user throw data to the user_throws table
    public function addUserThrow($userId, $score, $throwDuration){

        $added = new Added();

        $query="INSERT INTO user_throws(user_id, score, throwduration) VALUES (?,?,?)";
        $stmt = $this->mysqli->prepare($query);
        if($stmt){
            $stmt->bind_param('iii', $userId, $score, $throwDuration);
            $stmt->execute();
            $insertId = $stmt->insert_id;
            $stmt->free_result();
            $stmt->close();

            $added->id = $insertId;
        }

        return $added;
    }
    //this method is responsible for getting the user throws that the user has made by user id
    public function getUserThrowsByUserId($userId){

        $array = array();

        $query = "SELECT * FROM user_throws WHERE user_id = (?) ORDER BY score DESC LIMIT 10";
        $stmt = $this->mysqli->prepare($query);
        if($stmt){
            $stmt->bind_param('i', $userId);
            $stmt->execute();

            $stmt->bind_result($id, $userId, $score, $throwDate, $throwDuration);
            while($stmt->fetch()){

                $userThrow = new UserThrow();
                $userThrow->id = $id;
                $userThrow->userId = $userId;
                $userThrow->score = $score;
                $userThrow->throwDate = $throwDate;
                $userThrow->throwDuration = $throwDuration;

                $array[sizeof($array)] = $userThrow;


            }
            $stmt->free_result();
            $stmt->close();
        }

        return $array;
    }
    //this method is responsible for deleting a leaderboard entry by id
    public function deleteLeaderboardById($id){
        $deleted = new Delete();
        $query = "DELETE FROM leaderboard WHERE id = ?";
        $stmt = $this->mysqli->prepare($query);
        if($stmt){
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->free_result();
            $stmt->close();
            //todo make sure that it was deleted
        }

        return $deleted;
    }
    //this method is responsible for getting the throwstatistics which holds the statistics for all throws by all users
    public function getThrowStatistics(){
        $administrator = null;
        $throwStats = new ThrowStatistics();
        $query = "SELECT * FROM throw_statistics";
        $stmt = $this->mysqli->prepare($query);
        if($stmt){
            $stmt->execute();
            $stmt->bind_result($id,$sumOfAllScores, $numberOfSubmissions, $sumOfAllScoresSquared);
            $stmt->fetch();

            $throwStats = new ThrowStatistics();
            $throwStats->id = $id;
            $throwStats->sumOfAllScores = $sumOfAllScores;
            $throwStats->numberSubmissions = $numberOfSubmissions;
            $throwStats->sumOfAllScoresSquared = $sumOfAllScoresSquared;

            $stmt->free_result();
            $stmt->close();
        }

        return $throwStats;
    }
    //this method is responsible for updating the throw_statistics table.  It will either insert if there is no entry in the table or update if an entry exists in the table
    public function updateThrowStatistics($throwStatistics){

        //see if throw statistics id is zero, if so then this is the first throw and we need to insert
        if($throwStatistics->id == 0){

            $query="INSERT INTO throw_statistics(sumofallscores, numbersubmissions) VALUES (?,?)";
            $stmt = $this->mysqli->prepare($query);
            if($stmt){
                $stmt->bind_param('iii', $throwStatistics->sumOfAllScores, $throwStatistics->sumOfAllScoresSquared, $throwStatistics->numberSubmissions);
                $stmt->execute();
                $insertId = $stmt->insert_id;
                $stmt->free_result();
                $stmt->close();
                if($insertId == 0){
                    return false;
                }
                return true;
            }

            return false;
        }
        else{
            //otherwise there is an entry in the throw statistics database and we should update
            $updated = new Updated();
            $query = "UPDATE throw_statistics SET sumofallscores=(?), numbersubmissions=(?), sumofallscoressquared =(?) WHERE id = (?)";
            $stmt = $this->mysqli->prepare($query);
            if($stmt){
                $stmt->bind_param('iiii', $throwStatistics->sumOfAllScores, $throwStatistics->numberSubmissions, $throwStatistics->sumOfAllScoresSquared, $throwStatistics->id);
                $stmt->execute();
                $number_affected = $stmt->affected_rows;
                $stmt->free_result();
                $stmt->close();
                $updated->id = $throwStatistics->id;
                $updated->numberUpdated = $number_affected;
                return true;
            }

            return false;
        }

    }
    //this method is responsible for getting all of the entries from the leaderboard table.  This is used to get all top throws for all countries
    public function getTopThrows(){

        $array = array();

        $query = "SELECT * FROM leaderboard LEFT JOIN user ON leaderboard.user_id=user.id ORDER BY score DESC";
        $stmt = $this->mysqli->prepare($query);
        if($stmt){
            $stmt->execute();
            $stmt->bind_result($leaderBoardId, $userId1, $score, $throwDate, $throwDuration, $userId2, $username, $joinDate, $country);
            while($stmt->fetch()){

                $leaderBoard = new Leaderboard();
                $leaderBoard->id = $leaderBoardId;
                $leaderBoard->userId = $userId1;
                $leaderBoard->score = $score;
                $leaderBoard->throwDate = $throwDate;
                $leaderBoard->throwDuration = $throwDuration;
                $user = new User();
                $user->id = $userId1;
                $user->userName = $username;
                $user->joinDate = $joinDate;
                $user->country = $country;

                $leaderBoardUserContainer = new LeaderBoardUserContainer();
                $leaderBoardUserContainer->user = $user;
                $leaderBoardUserContainer->leaderboard = $leaderBoard;

                $array[sizeof($array)] = $leaderBoardUserContainer;


            }
            $stmt->free_result();
            $stmt->close();
        }

        return $array;
    }
    //this method is responsible for getting top throws from the leaderboard table by country
    public function getTopThrowsByCountry($country){

        $array = array();
        $query = "SELECT * FROM leaderboard LEFT JOIN user ON leaderboard.user_id=user.id WHERE country = (?) ORDER BY score DESC";
        $stmt = $this->mysqli->prepare($query);
        if($stmt){
            $stmt->bind_param('s', $country);
            $stmt->execute();
            $stmt->bind_result($leaderBoardId, $userId1, $score, $throwDate, $throwDuration, $userId2, $username, $joinDate, $country);
            while($stmt->fetch()){

                $leaderBoard = new Leaderboard();
                $leaderBoard->id = $leaderBoardId;
                $leaderBoard->userId = $userId1;
                $leaderBoard->score = $score;
                $leaderBoard->throwDate = $throwDate;
                $leaderBoard->throwDuration = $throwDuration;

                $user = new User();
                $user->id = $userId1;
                $user->userName = $username;
                $user->joinDate = $joinDate;
                $user->country = $country;

                $leaderBoardUserContainer = new LeaderBoardUserContainer();
                $leaderBoardUserContainer->user = $user;
                $leaderBoardUserContainer->leaderboard = $leaderBoard;

                $array[sizeof($array)] = $leaderBoardUserContainer;

            }
            $stmt->free_result();
            $stmt->close();
        }

        return $array;
    }
}