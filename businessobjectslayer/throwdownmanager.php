<?php
/*
This is our ThrowdownManager class which is responsible for all of our business object layer responsibilities.
This class sits in between the webservice layer and the data access layer.  Methods from this class are called by
the web service layer and the manager methods will get data needed from the data access layer.  The methods will then
process the raw data obtained from the data access layer and return it to the webservice layer
 */
class ThrowdownManager{

    //This method registers new users who have installed the app on their mobile phone.
    public function registerUserAccount($userName, $country){
        $userName = str_replace(' ', '', $userName);
        $country = str_replace(' ', '', $country);

        $throwdownDao = new ThrowdownDAO();

        //first need to see if this name exists
        $number = $throwdownDao->countUserByUsername($userName);
        if($number != 0){
            //if the user name is taken return null so the user will have to choose a different username on the mobile front end
            return NULL;
        }
        //user name was not taken so pass this username and country off to the data access layer which will insert it into the database
        return $throwdownDao->addUserAccount($userName, $country);
    }
    //this method updates a user account.
    public function updateUserAccount($userId, $country, $userName){

        $throwdownDao = new ThrowdownDAO();

        //need to see if username is taken
        $number = $throwdownDao->countUserByUsername($userName);
        if($number != 0){
            //then there is a username by this name and we should return that the username/country was not updated
            $updated = new Updated();
            $updated->id = 0;
            return $updated;
        }
        //the proposed username is not taken so we should pass the username/country off to the webservice layer to be updated
        return $throwdownDao->updateUser($userId, $country, $userName);

    }
    //this is a method that is used to calculate the user throw percentile which calculates how well the user's throw was compared to all user throws recorded
    private function calculateThrowPercentile($score){

        //need to get the current throw statistics which is the statistics for all user throws recorded by all users
        $throwDao = new ThrowdownDAO();
        $throwStats = $throwDao->getThrowStatistics();
        $throwStats->numberSubmissions++;


        $scoreSquared = $score * $score;
        $throwStats->sumOfAllScoresSquared = $throwStats->sumOfAllScoresSquared + $scoreSquared;


        //need to update the throw statistics with data from this new score
        $sumOfAllScores = $throwStats->sumOfAllScores;
        $newSumOfAllScores = $sumOfAllScores + $score;
        $throwStats->sumOfAllScores = $newSumOfAllScores;
        //pass new statistics off to data access layer for updating
        $throwDao->updateThrowStatistics($throwStats);


        //calculate the percentile of this user's throw against all user throws
        $sumOfAllScoresSquared = $throwStats->sumOfAllScoresSquared;
        $meanOfAllScores = $throwStats->sumOfAllScores/$throwStats->numberSubmissions;
        $numberOfScores = $throwStats->numberSubmissions;
        $mathManager = new MathManager();
        $percentile = $mathManager->calculatePercentile($score, $sumOfAllScoresSquared, $meanOfAllScores, $sumOfAllScores, $numberOfScores);
        return ceil($percentile);

    }

    //get all the user's throws that have been recorded in the database.
    public function getUserThrowsByUserId($userId){
        $throwdownDao = new ThrowdownDAO();
        return $throwdownDao->getUserThrowsByUserId($userId);
    }
    //here is where we add a new throw for a user to the database
    public function addUserThrowScore($userId, $score, $throwDuration){

        $throwdownDao = new ThrowdownDAO();
        //first need to make sure that the user exists
        $user = $throwdownDao->getUserByUserId($userId);
        if($user == null || $user->id == 0){
            $userThrowAddDetails = new UserThrowAddDetails();
            return $userThrowAddDetails;
        }
        //This is our data transfer object wrapper that will hold our values that will be added, both for user throws and leaderboard tables
        $userThrowAddDetails = new UserThrowAddDetails();

        //first add the throw info to the user throw table
        $userThrowAdded = $throwdownDao->addUserThrow($userId, $score, $throwDuration);
        $userThrowAddDetails->userThrowAdded = $userThrowAdded;





        /*
        need to only add to the leaderboard if the score is greater than the top 50
        get the top 50 and if there are less than 50 add to table, if greater than 50 then see if it beats lowest score
        if so then add to table and delete lowest score from table
        */
        $throwsArray = $throwdownDao->getTopThrows();
        if(sizeof($throwsArray) < 50){
            //then we can add the throw to the table
            $added = $throwdownDao->addUserThrowDataToLeaderBoard($userId, $score, $throwDuration);
            $userThrowAddDetails->leaderBoardAdded = $added;
        }
        else{
            $leaderboardUserContainer = $throwsArray[sizeof($throwsArray) - 1];
            $leaderBoard = $leaderboardUserContainer->leaderboard;
            if($leaderBoard->score < $score){
                //then the user threw a score greater than the lowest score, so we should remove the lowest score and add this new score
                //remove lowest score
                $throwdownDao->deleteLeaderBoardById($leaderBoard->id);
                //now add this new score
                $added = $throwdownDao->addUserThrowDataToLeaderBoard($userId, $score, $throwDuration);
                $userThrowAddDetails->leaderBoardAdded = $added;

            }
            else{
                $added = new Added();
                $added->id = 0;
                $userThrowAddDetails->leaderBoardAdded = $added;
            }
        }
        //need to determine what the percentile is, then return add it to the user throw add detail DTO
        $scorePercentile = $this->calculateThrowPercentile($score);
        $userThrowAddDetails->percentile = $scorePercentile;

        return $userThrowAddDetails;
    }

    //this method gets the leaderboard data for all countries. This contains the top 50 (if that many entries in the leaderboard table) entries in the leaderboard table
    public function getWorldLeaderBoardData(){
        $throwdownDao = new ThrowdownDAO();
        $leaderBoardUserContainerArray = $throwdownDao->getTopThrows();
        return $leaderBoardUserContainerArray;
    }
    //this method gets the leaderboard entries by country.
    public function getCountryLeaderBoardData($country){
        $throwdownDao = new ThrowdownDAO();
        $leaderBoardUserContainerArray = $throwdownDao->getTopThrowsByCountry($country);
        return $leaderBoardUserContainerArray;
    }
}

























