<?php

/*
 This class is responsible for getting the database access credentials which will allow database access.
 */
class DatabaseUtil{
    //this method gets the database credentials from our databaseinfo.php file
    static function getDatabaseConfigurationDetails(){

        $databaseConfigurationInformation = new DatabaseConfigurationInformation();

        $fileName = "../conf/databaseinfo.php";

        try{
            $contents = file($fileName);
        }
        catch(Exception $e){
            return null;
        }
        foreach($contents as $key => $value)
        {
            if(strpos($value, "server") !== false)
                $serverstring = $value;
            if(strpos($value, "database") !== false)
                $databasestring = $value;
            if(strpos($value, "username") !== false)
                $usernamestring = $value;
            if(strpos($value, "password") !== false)
                $passwordstring = $value;
        }
        $serverArray = explode("=", $serverstring);
        $server = trim($serverArray[1]);
        $databaseArray = explode("=", $databasestring);
        $database = trim($databaseArray[1]);
        $userArray = explode("=", $usernamestring);
        $user = trim($userArray[1]);
        $passwordArray = explode("=", $passwordstring);
        $password = trim($passwordArray[1]);
        $databaseConfigurationInformation->server = $server;
        $databaseConfigurationInformation->database = $database;
        $databaseConfigurationInformation->username = $user;
        $databaseConfigurationInformation->password = $password;
        return $databaseConfigurationInformation;
    }
}