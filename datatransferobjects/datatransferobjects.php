<?php

/*
 This file contains all of our data transfer object classes used to transfer data between the backend and the front end
 */

class User{
    public $id;
    public $userName;
    public $joinDate;
    public $country;
}
class Leaderboard{
    public $id;
    public $userId;
    public $score;
    public $throwDate;
    public $throwDuration;
}
class LeaderBoardUserContainer{
    public $user;
    public $leaderboard;
}
class UserThrowStats{
    public $userId;
    public $numThrows = 0;
    public $personalBest = 0;
}
class ThrowStatistics{
    public $id = 0;
    public $sumOfAllScores = 0;
    public $sumOfAllScoresSquared = 0;
    public $numberSubmissions = 0;
}
class UserThrowAddDetails{
    public $percentile;
    public $leaderBoardAdded;
    public $userThrowAdded;
}
class Success{
    public $success = true;
    public $data;
}
class Added{
    public $id;
}
class DatabaseConfigurationInformation{
    public $server;
    public $database;
    public $username;
    public $password;
}
class Updated {
    public $numberUpdated = 0;
    public $id;
}
class Delete {
    public $numberDeleted = 0;
    public $id;
}
class UserThrow{
    public $id;
    public $userId;
    public $score;
    public $throwDate;
    public $throwDuration;

}
