<?php
/*
 This is our MathManager class which is responsible for calculating the throw percentile which gives
how high the user's throw score was compared to all the throws recorded by all users for the app.
The algorithms for the erf and cdf were obtained directly from an online source.
 */
class MathManager{

    public function calculatePercentile($score, $sumOfAllScoresSquared, $meanOfAllScores, $sumOfAllScores, $numberOfScores){


        $standardDeviationSquared = ($sumOfAllScoresSquared - (2 * $meanOfAllScores * $sumOfAllScores) +
                                    ($numberOfScores * ($meanOfAllScores*$meanOfAllScores)))/($numberOfScores -1);
        $standardDeviation = pow($standardDeviationSquared, .5);

        $z = ($score - $meanOfAllScores) / $standardDeviation;
        $percentile = $this->cdf($z) * 100;
        return $percentile;

    }


    public function erf($x)
    {
        $pi = 3.1415927;
        $a = (8*($pi - 3))/(3*$pi*(4 - $pi));
        $x2 = $x * $x;

        $ax2 = $a * $x2;
        $num = (4/$pi) + $ax2;
        $denom = 1 + $ax2;

        $inner = (-$x2)*$num/$denom;
        $erf2 = 1 - exp($inner);

        return sqrt($erf2);
    }

    public function cdf($n)
    {
        if($n < 0)
        {
            return (1 - $this->erf($n / sqrt(2)))/2;
        }
        else
        {
            return (1 + $this->erf($n / sqrt(2)))/2;
        }
    }
}