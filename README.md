
This is the repository for the Mobile app game called
Throwdown.  This backend is written in php.
This repository has all of the 'backend' server code.  
The backend server code is run on an apache server and consists
of three modules:
1) Webservice Layer
2) Business Object Layer
3) Data Access Layer


Webservice Layer
---------------------
The Webservice Layer is a RESTful webservice that handles
all of the calls from every mobile app.  This layer makes calls
to the business object layer

Business Object Layer
----------------------
The business object layer gets called from the webservice layer
and makes calls to the data access layer. It gets data back from 
the data access layer and processes it so it is in the correct
format for the webservice layer which it returns the data to.

Data Access Layer
----------------------
The data access layer is responsible for making all the calls
to the database.  This layer is called from the business object
layer and makes calls to the mysql database.

