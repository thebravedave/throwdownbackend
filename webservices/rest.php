<?php

/*
 *
 *
 *
This php file is where we define our RESTful webservice layer.
Each $app->get(....) call represents a single webservice endpoint that is
called from the shared code on the mobile application
 *
 *
 */

error_reporting(E_ALL);
ini_set('display_errors', '1');

session_start();

include_once("../datatransferobjects/datatransferobjects.php");
include_once("../businessobjectslayer/mathmanager.php");
include_once("../businessobjectslayer/throwdownmanager.php");
include_once("../util/databaseutil.php");
include_once("../dataaccesslayer/throwdowndao.php");


require '../lib/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    'debug' => true
));

$app->get('/user/account/registration/:username/:country', function ($userName, $country) use($app) {

    $success = new Success();

    $throwdownManager = new ThrowdownManager();
    $added = $throwdownManager->registerUserAccount($userName, $country);
    if($added == NULL || $added->id == NULL || $added->id == 0){
        $success->success = false;
        $added = new Added();
        $added->id = 0;
        $success->data = $added;
    }
    else{
        $success->data = $added;
    }
    $json = json_encode($success);
    echo $json;

});//user/account/registration/:username/:country

$app->get('/user/account/update/:userId/:country/:userName', function($userId, $country, $userName) use($app) {

    $throwdownManager = new ThrowdownManager();

    $updated = $throwdownManager->updateUserAccount($userId, $country, $userName);
    $success = new Success();
    if($updated == null){
        $success->success = false;
        $success->data = $updated;
    }
    else if($updated->numberUpdated == 0){
        $success->success = false;
        $success->data = $updated;
    }
    else{
        $success->data = $updated;
    }

    $json = json_encode($success);
    echo $json;

}); //user/account/update/:userId/:country/:userName


$app->get('/user/throw/add/:userId/:score/:throwduration', function($userId, $score, $throwDuration) use($app) {

    $throwdownManager = new ThrowdownManager();
    $score = (int) $score;
    $userThrowAddDetails = $throwdownManager->addUserThrowScore($userId, $score, $throwDuration);
    $success = new Success();
    $success->data = $userThrowAddDetails;

    $json = json_encode($success);
    echo $json;

}); //user/throw/add/:userId/:score/:throwduration


$app->get('/user/throws/:userId', function($userId) use($app) {
    $userId = (int) $userId;
    $throwdownManager = new ThrowdownManager();
    $userThrows = $throwdownManager->getUserThrowsByUserId($userId);
    $success = new Success();
    $success->data = $userThrows;

    $json = json_encode($success);
    echo $json;

}); //user/throw/:userId


$app->get('/leaderboard/world/', function() use($app) {

    $throwdownManager = new ThrowdownManager();
    $usersThrowStats = $throwdownManager->getWorldLeaderboardData();
    $success = new Success();

    if($usersThrowStats == null){
        $success->success = false;
    }
    else{
        $success->data = $usersThrowStats;
    }

    $json = json_encode($success);
    echo $json;

}); //leaderboard/world/

$app->get('/leaderboard/country/:country', function($country) use($app) {

    $throwdownManager = new ThrowdownManager();
    $usersThrowStats = $throwdownManager->getCountryLeaderboardData($country);
    $success = new Success();

    if($usersThrowStats == null){
        $success->success = false;
    }
    else{
        $success->data = $usersThrowStats;
    }

    $json = json_encode($success);
    echo $json;

}); //leaderboard/country/:country




$app->run();

